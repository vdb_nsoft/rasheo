# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime

from django.db import models

from django.utils import timezone

import django

# from base64 import b64encode, b64decode
# from Crypto.Cipher import ARC4


# Create your models here.


class User(models.Model):
    first_name = models.CharField(max_length=15, blank=False)
    last_name = models.CharField(max_length=15, blank=False)
    dob = models.DateField(blank=False)
    MALE = 'M'
    FEMALE = 'F'
    SEX_CHOICES = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )
    sex = models.CharField(
        max_length=1,
        choices=SEX_CHOICES,
        blank=False,
    )
    race = models.CharField(max_length=15, blank=False)
    cont_no = models.CharField(max_length=13, blank=False)
    email = models.CharField(max_length=20, blank=False)
    height = models.FloatField(default=0.0, blank=False)
    weight = models.FloatField(default=0.0, blank=False)
    username = models.CharField(max_length=15, blank=False)
    password = models.CharField(max_length=15, blank=False)
    STATUS_CHOICES = (
        (1, 'Active'),
        (0, 'Inactive'),
    )
    status = models.IntegerField(
        choices=STATUS_CHOICES,
        default=1,
    )
    created_at = models.DateTimeField(default=datetime.datetime.today())
    created_by = models.IntegerField(blank=False)
    IS_DELETED_CHOICES = (
        (1, 'Yes'),
        (0, 'No'),
    )
    is_deleted = models.IntegerField(
        choices=IS_DELETED_CHOICES,
        default=0,
    )
